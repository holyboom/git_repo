import { FETCH_REPO, FETCHING_REPO, FETCH_ERROR } from "../actions/types";

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_REPO:
      return Object.assign([], action.payload.items, {
        isFetching: false,
        isError: false,
        didFetch: true,
      })

    case FETCHING_REPO:
      return Object.assign([], [], {
        isFetching: true,
        isError: false,
      })
    case FETCH_ERROR:
      return Object.assign([], action.payload.data, {
        isFetching: false,
        isError: true,
      })
  }
  return state;
}
