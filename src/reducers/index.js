import { combineReducers } from "redux";
import RepoReducer from "./reducer_repo";
import PageReducer from "./reducer_page";


const rootReducer = {
  repo: RepoReducer,
  totalRepos: PageReducer,
};

export default rootReducer;
