import { FETCH_PAGE } from "../actions/types";

export default function(state = null, action) {
  switch (action.type) {
    case FETCH_PAGE:
      return action.payload.total_count;
  }
  return state;
}
