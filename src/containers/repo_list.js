import React, { Component } from "react";
import { connect } from "react-redux";
import ReactLoading from 'react-loading';
import Timestamp from 'react-timestamp';

class RepoList extends Component {
  constructor(props){
    super(props)
    this.state = {
      initialState : true
    }
  }

  renderRepo(repo) {
    const index = repo.id;
    const name = repo.name;
    const owner = repo.owner.login;
    const descript = repo.description;
    const last_update = repo.update_at;
    const stars_count = repo.stargazers_count;
    const forks_count = repo.forks_count;
    const repo_url = repo.html_url;
    const updated_at = repo.updated_at;
    const language = repo.language;

    let styles = {

       styleOwner : {
        marginBottom: 0,
        paddingLeft: 10,
        color: "#aaa"
      },

       paddingLeft : {
        paddingLeft: 20,
      },

       resetLRPadding : {
        paddingLeft: "0 !important",
        paddingRight: "0 !important"
      },

        resetTagA : {
          textDecoration: "none",
          color: "#000"
        }
    }


    return(
      <li key={index} className="list-group-item">
        <a href={repo_url} target="_blank" style={styles.resetTagA}>
          <div className="d-flex p-2 align-items-center" style={styles.resetLRPadding}>
            <h4>{name}</h4>
            <p style={styles.styleOwner}> by {owner}</p>
          </div>
          <p className={language === null ? "" : "created_tag"}>
            {language}
          </p>
          <div className="col-12" style={styles.resetLRPadding}>
            <p>{descript}</p>
          </div>
          <div className="d-flex p-2 align-items-center">
            <p className="responsive-font">
              <i className="fa fa-star" aria-hidden="true"></i>
              {stars_count.toLocaleString()}
            </p>
            <p className="responsive-font" style={styles.paddingLeft}>
              <i className="fa fa-code-fork" aria-hidden="true"></i>
              {forks_count.toLocaleString()}
            </p>
            <p className="responsive-font" style={styles.paddingLeft}>
              Last commit :
              <Timestamp time={updated_at} utc={false}/>
            </p>

          </div>
        </a>
      </li>
    )
  }


  isEmptyRepo(){
   if(this.props.repo.length === 0 && this.props.repo.didFetch && !this.props.repo.isFetching && !this.props.repo.isError){
     return (
       <h2 className="text-center">No Packages Found :(</h2>
     )
   }
 }


  isError(){
    return (this.props.repo.isError ?
      <h4 className="rounded border-1">
        {this.props.repo.message}
      </h4>
      : null
    )
  }

  render() {
    return (
      <div className="container col-md-8">
        <ul className="group-list resetPadding">
          <ReactLoading
            type="bars"
            color="#444"
            className={
              this.props.repo.isError || !this.props.repo.isFetching || !this.state.initialState ?
               "hidden-xs-up loading" : "loading"
             }
          />
          {this.isError()}
          {this.isEmptyRepo()}
          {this.props.repo.map(this.renderRepo)}
        </ul>
      </div>
    );
  }
}

function mapStateToProps({repo}) {
    return  { repo }
}

export default connect(mapStateToProps)(RepoList);
