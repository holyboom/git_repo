import React, { Component } from "react";
import { Pagination } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchUrlPage } from "../actions/index";
import { withRouter } from 'react-router-dom';

class PaginationAdvanced extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activePage: 1
    }

    this.handleSelect = this.handleSelect.bind(this);
  }

  filterItems(totalRepos){
    totalRepos =  Math.ceil(totalRepos/10)
    return (totalRepos > 100) ? 100 : totalRepos
  }

  componentWillMount(){
    let query = new URLSearchParams(this.props.location.search);
    let activePage = parseInt(query.get('page'),10);
    let search = query.get('q');

    if(activePage) {
      this.setState({ activePage });
      let path = `/search?q=${search}&page=${activePage}`;
      this.props.history.push(path);
      this.props.fetchUrlPage(activePage)
    }
  }

  handleSelect(event) {

    let query_repo = new URLSearchParams(this.props.location.search);
    let stringRepo = query_repo.get('q');
    let path_page = `/search?q=${stringRepo}&page=${event}`;
    this.props.history.push(path_page);

    let query_page = new URLSearchParams(this.props.location.search);
    let currentPage = query_page.get('page');
    this.setState({ term: currentPage });
    this.props.fetchUrlPage(event)
    this.setState({ activePage: event })

  }

  render() {
    return (
      <Pagination
        first
        prev={"previous"}
        next={"next"}
        last
        ellipsis
        boundaryLinks
        items={this.filterItems(this.props.totalRepos)}
        maxButtons={5}
        activePage={this.state.activePage}
        onSelect={this.handleSelect}
        className={this.props.totalRepos === 0 ? 'hidden-xs-up':'shown justify-content-center custom-pagination'}

      />
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchUrlPage }, dispatch);
}

function mapStateToProps({totalRepos}) {
    return  { totalRepos }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PaginationAdvanced));
