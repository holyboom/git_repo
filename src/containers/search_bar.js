import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchRepo } from "../actions/index";
import { withRouter } from 'react-router-dom';

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: "react" };

    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentWillMount(){
    const query = new URLSearchParams(this.props.location.search);
    const term = query.get('q');

    if(term) {
      this.setState({ term });
      const path = `/search?q=${term}&page=1`;
      this.props.history.push(path);
      this.props.fetchRepo(term);
    }
  }

  onInputChange(event) {
    this.setState({ term: event.target.value });
  }

  onFormSubmit(event) {
    event.preventDefault();
    let path = `/search?q=${this.state.term}&page=1`;
    this.props.history.push(path);
    this.props.fetchRepo(this.state.term);
  }

  render() {


    return (
      <form onSubmit={this.onFormSubmit} className="input-group">
        <input
          placeholder="Search GitHub"
          className="form-control"
          value={this.state.term}
          onChange={this.onInputChange}
          required
        />
        <span className="input-group-btn">
          <button type="submit" className={this.state.term != "" ? 'btn btn-success' : "btn btn-warning"}>Submit</button>
        </span>
      </form>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchRepo }, dispatch);
}

export default withRouter(connect(null, mapDispatchToProps)(SearchBar));
