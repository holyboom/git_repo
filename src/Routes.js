import React from 'react';
import App from './components/App';
import HomePage from './components/HomePage';
import NotFoundPage from './components/NotFoundPage';

export default [
  {
    ...App,
    routes: [
      {
        ...HomePage,
        path: '/'
      },
      {
        ...NotFoundPage
      }
    ]
  }
];
