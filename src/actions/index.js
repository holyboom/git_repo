import axios from "axios";
import {
  FETCH_REPO,
  FETCH_PAGE
} from './types';

const ROOT_URL = `https://api.github.com/search/repositories`;
let currentUrl = "";

export function fetchRepo(search) {
  const url = `${ROOT_URL}?q=${search}&sort=stars&order=desc&per_page=10&page=1`;
  currentUrl = `${ROOT_URL}?q=${search}&sort=stars&order=desc&per_page=10&page=`;

  const request = axios.get(url)
    return (dispatch) => {
      dispatch({ type: 'FETCHING_REPO'})
      request.then(
        ({data}) => {
          dispatch({ type: 'FETCH_REPO', payload: data })
          dispatch({ type: 'FETCH_PAGE', payload: data })
        },
        error => {
          console.log(error);
          dispatch({ type: 'FETCH_ERROR', payload: error })
          throw error
        }
      )
    }
}

export function fetchUrlPage(nextPage) {
  const url = `${currentUrl}${nextPage}`;

  const request = axios.get(url)
    return (dispatch) => {
      dispatch({ type: 'FETCHING_REPO'})
      request.then(
        ({data}) => {
          dispatch({ type: 'FETCH_REPO', payload: data })
          dispatch({ type: 'FETCH_PAGE', payload: data })
        },
        error => {
          console.log(error);
          dispatch({ type: 'FETCH_ERROR', payload: error })
          throw error
        }
      )
    }
}
