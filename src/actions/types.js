export const FETCH_REPO = "FETCH_REPO";
export const FETCHING_REPO = "FETCHING_REPO";
export const FETCH_ERROR = "FETCH_ERROR";
export const FETCH_PAGE = "FETCH_PAGE";
