import React, { Component } from "react";
import PaginationAdvanced from "../containers/PaginationAdvanced";
import SearchBar from "../containers/search_bar";
import RepoList from "../containers/repo_list";

const Home = () => {
  return (
    <div>
      <SearchBar />
      <PaginationAdvanced />
      <RepoList />
    </div>
  );
};

export default {
  component: Home
};
