import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { createStore, combineReducers, applyMiddleware } from "redux";
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'
import thunk from 'redux-thunk';
import axios from 'axios';
import reducers from './reducers';
import Routes from './routes';

import App from "./components/App";

const axiosInstance = axios.create({
  baseURL: '/api'
});
const history = createHistory()
const middleware = routerMiddleware(history)

const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer
  }),
  window.INITIAL_STATE,
  applyMiddleware(thunk.withExtraArgument(axiosInstance), middleware)
);

ReactDOM.hydrate(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>{renderRoutes(Routes)}</div>
    </ConnectedRouter>
  </Provider>,
  document.querySelector(".container")
);
