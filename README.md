# Git Repo

GitHub API to list all public repositories

###Getting Started###

Step 2

This second step focuses on communication with an external API.

Use the GitHub API to list all public repositories.
Show 10 repositories in a table with all the information that you judge necessary.
Add pagination to allow the user to navigate the repositories, 10 by 10.

###Getting Started###

There are two methods for getting started with this repo.

####Familiar with Git?#####
Checkout this repo, install depdencies, then start the gulp process with the following:

```
	> git clone holyboom@bitbucket.org:holyboom/git_repo.git
	> cd git_repo
	> npm install
	> npm start
```

####Not Familiar with Git?#####
Click [here](https://bitbucket.org/holyboom/git_repo) then download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
	> npm install
	> npm start
```



